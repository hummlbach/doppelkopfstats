import math
from player import *
from game import *

# constants
C_CONST = 50
LAMBDA_CONST = 0.045


def calculateRatingChangesForSingleGame(game: Game, playersDict: Player):
    """

    :param dict game: Dictionary containing information about players participated in game as well as the number of points each one has achieved
    """

    numberOfPlayers = len(game.players)
    initialRatings = dict()
    ratingChanges = dict()

    initialRatingsSum = 0
    # for playerInfo in stats:
    for playerName, player in playersDict.items():
        # check if player was part of the game
        if playerName not in game.players:
            continue
        # playerName = playerInfo["Player"]
        # initialRating = sum(player.piRatingPerGame.values())
        piRatings = [*player.piRatingPerGame.values()]
        if len(piRatings) == 0:
            initialRating = 0
        else:
            initialRating = piRatings[-1]  # newest rating is initial#
        # print(f"Initial rating for {playerName} is {initialRating}")

        # set initial rating values
        initialRatings[playerName] = initialRating
        initialRatingsSum += initialRating
        # prepare dict for rating changes
        ratingChanges[playerName] = 0

    for playerName, player in playersDict.items():
        # check if player was part of the game
        if playerName not in game.players:
            continue
        # player = playerInfo["Player"]
        # actualPoints = playerInfo["Points"]
        actualPoints = game.getPointsOfPlayer(playerName)
        avgOpponentRating = (initialRatingsSum - initialRatings[playerName]) / (numberOfPlayers - 1)
        expectedPoints = calculateExpectedPoints(initialRatings[playerName], avgOpponentRating)
        # print(f"{playerName}: Actual Points: {actualPoints}, Expected points: {expectedPoints} (avg. opp. rating: {avgOpponentRating})")

        e = abs(actualPoints - expectedPoints)

        psi = psiFunction(e)

        if actualPoints > expectedPoints:
            psiA = psi
            psiG = -psi
        else:
            psiA = -psi
            psiG = psi
        # print(f"psi: {psiA} => rating change: {psiA *lambdaConst}")

        ratingChanges[playerName] = ratingChanges[playerName] + psiA * LAMBDA_CONST
        # also adapt ratings for all other players
        for oppontentPlayer in initialRatings:
            if oppontentPlayer != playerName:
                ratingChanges[oppontentPlayer] = (
                    ratingChanges[oppontentPlayer] + psiG * LAMBDA_CONST / numberOfPlayers
                )
        # print(f"Rating changes after processing player \"{player}\":\n{ratingChanges}")

    # return ratingChanges
    # store rating changes
    for playerName, change in ratingChanges.items():
        playersDict[playerName].piRatingPerGame[game.date] = change + initialRatings[playerName]


def psiFunction(e):
    return C_CONST * math.tanh(e / C_CONST)


def artanh(y):
    return 0.5 * math.log((1 + y) / (1 - y))


def expectedPoints(rating):
    """ """
    if abs(rating) < 0.99 * C_CONST:
        return C_CONST * artanh(abs(rating) / C_CONST)
    else:
        return C_CONST * artanh(0.99)


def calculateExpectedPoints(playerRating, avgOpponentRating):
    """ """

    PA = expectedPoints(playerRating)
    PGT = expectedPoints(avgOpponentRating)

    if playerRating < 0:
        PA = -PA
    if avgOpponentRating < 0:
        PGT = -PGT

    # calc expected points fur current player
    return PA - PGT
