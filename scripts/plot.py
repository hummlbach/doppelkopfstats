import numpy as np
import hashlib
import matplotlib.pyplot as plt
from enum import Enum

from highscore import *
from general import StatsTypes
from player import isTeamName, getPlayersInTeam


class Color:
    def __init__(self, hexColor: int = None, red=None, green=None, blue=None):
        if hexColor == None and (red == None or green == None or blue == None):
            raise Exception("R, G and B of color must be provided in case hex code is not")
        elif hexColor != None and (red != None or green != None or blue != None):
            raise Exception(
                f"Only RGB or hex color code should be provided (hex={hexColor}, RGB={red} {green} {blue})"
            )
        if hexColor:
            self.hex = hexColor
            self.red = hexColor >> 16
            self.green = (hexColor >> 8) & 0xFF
            self.blue = hexColor & 0xFF
        else:
            self.hex = red << 16 | green << 8 | blue
            self.red = red
            self.green = green
            self.blue = blue

    def __str__(self):
        return f"{self.getHexString()} (R={self.red}, G={self.green}, B={self.blue})"

    def getHexString(self):
        return f"#{self.hex:06x}"


PLAYERS_COLOR = {
    "Arvid": Color(hexColor=0x1F77B4),
    "Felix": Color(hexColor=0xD62728),
    "Johannes": Color(hexColor=0x9467BD),
    "Benedikt": Color(hexColor=0x00186A),
    "Thomas": Color(hexColor=0xFF7514),
    "Michael": Color(hexColor=0x00C4C4),
    "Suresh": Color(hexColor=0x00805E),
    "Daniel H.": Color(hexColor=0x339933),
    "Daniel B.": Color(hexColor=0xFFFF00),
}


class PlotTypes(Enum):  # string value is y_label
    Points = "Points"
    Hands = "Hands"
    PiRating = "PiRating"
    ppHands = "Points per Hand"


class PlotData:
    def __init__(self, label, xLabel, type: PlotTypes):
        self.label = label
        self.is_team = isTeamName(label)
        self.xLabel = xLabel
        self.x = set()
        self.y = list()
        self.y_cumulative = list()
        self.annotate_decimal_places = 0
        self.type = type

    def __str__(self):
        return (
            f"Plot data for {self.label} with\n"
            f"x-values: {self.x}\n"
            f"y-values: {self.y}\n"
            f"y-values cumulative: {self.y_cumulative}\n"
            f"type: {self.type}\n"
            f"xLabel: {self.xLabel}\n"
        )

    def setAnnotationDecimalPlaces(self, decimalPlaces):
        self.annotate_decimal_places = decimalPlaces

    def calcCumuluativeSumY(self):
        self.y_cumulative = np.cumsum(self.y)

    def plot(self, setXTicks=False):
        if self.type not in [PlotTypes.PiRating, PlotTypes.ppHands]:
            self.calcCumuluativeSumY()
        sortedX = list(self.x)
        sortedX.sort()
        (legendHandle,) = plt.plot(
            sortedX,
            self.y_cumulative,
            ".-",
            # remove underscores in player-name (relevant for teams)
            label=self.label.replace("_", " "),
            color=playerToHexColor(self.label, mixColors=False),
        )
        # print points at newest datapoint
        if self.annotate_decimal_places != 0:
            annotation = np.round(self.y_cumulative[-1], decimals=self.annotate_decimal_places)
        else:
            annotation = self.y_cumulative[-1]
        plt.annotate(
            f"{annotation} P.",
            xy=(sortedX[-1], annotation),
            xytext=(3, 3),
            textcoords="offset points",
        )

        plt.xlabel(self.xLabel)
        if setXTicks:
            plt.xticks(list(self.x))
            plt.gcf().autofmt_xdate()

        return legendHandle


def plotHighscoreAsBar(highscore, filename):
    fig = plt.figure(figsize=[30, 8])
    # set main title for figure
    fig.suptitle(highscore.title, fontsize=16)
    # plot sub plots
    for k, (dateType, tupleData) in enumerate(highscore.getData().items()):
        plt.subplot(1, len(highscore.data), k + 1)
        # set ylabel for first plot (most left)
        plt.ylabel(highscore.ylabel)
        plt.title(dateType)
        if isTeamName(tupleData[0][0]):  # TODO: make it more transparent
            width = 0.6
        else:
            width = 0.8
        barlist = plt.bar(
            list(range(0, len(tupleData))), [tuple[1] for tuple in tupleData], width=width
        )
        for k in range(len(tupleData)):
            name = tupleData[k][0]  # TODO: make it more transparent
            if isTeamName(name):
                players = getPlayersInTeam(name)
                colorA = playerToHexColor(players[0], mixColors=False)
                colorB = playerToHexColor(players[1], mixColors=False)
            else:
                colorA = playerToHexColor(name, mixColors=False)
            barlist[k].set_color(colorA)
            if isTeamName(name):
                # increase thickness of edge
                barlist[k].set_linewidth(20 - len(tupleData))
                barlist[k].set_edgecolor(colorB)
        # label values
        autolabel(barlist, highscore.dataIsFloat)
        # set player names as x-ticks
        ax = plt.gca()
        ax.set_xticks(list(range(0, len(tupleData))))
        # enable grid
        ax.grid(zorder=0)
        if dateType == StatsTypes.OVERALL:
            ax.set_xticklabels(
                [tuple[0].replace("_", " ") for tuple in tupleData], rotation=65, fontsize=10
            )
        else:
            xTickLabels = []
            for e in tupleData:
                xTickLabels.append(e[0].replace("_", " ") + "\n(" + e[2] + ")")
            ax.set_xticklabels(xTickLabels, rotation=65)
            plt.setp(ax.get_xticklabels(), fontsize=10)

    plt.savefig(filename.with_suffix(".png"), bbox_inches="tight")
    plt.close(fig)


def playerToHexColor(playerOrTeam, mixColors):
    if not mixColors:
        return playerToRGBColor(playerOrTeam).getHexString()
    else:
        players = getPlayersInTeam(playerOrTeam)
        colorPlayerA = playerToRGBColor(players[0])
        colorPlayerB = playerToRGBColor(players[1])
        return mixColors(colorPlayerA, colorPlayerB).getHexString()


def playerToRGBColor(playerName):
    # individual player colors => do not use calculated hex color
    if playerName in PLAYERS_COLOR:
        return PLAYERS_COLOR[playerName]

    # use only capital letters in name
    hashValue = int(hashlib.md5(playerName.upper().encode("utf-8")).hexdigest(), 16)
    return Color(hexColor=(hashValue % 0x1000000))


def mixColors(colorA, colorB):
    R = int((colorA.red + colorB.red) / 2)
    G = int((colorA.green + colorB.green) / 2)
    B = int((colorA.blue + colorB.blue) / 2)

    return Color(red=R, green=G, blue=B)


def autolabel(rects, isFloat):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax = plt.gca()
        if isFloat:
            label = float(round(height, 2))
        else:
            label = int(height)

        ax.text(
            rect.get_x() + rect.get_width() / 2.0,
            height,
            label,
            ha="center",
            va="bottom",
            fontsize=13,
        )
