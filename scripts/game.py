from general import HandTypes


class Game:
    def __init__(self, date, players, hands):
        self.date = date
        self.players = players
        self.hands = hands

    def __eq__(self, other):
        if isinstance(other, Game):
            return (
                self.date == other.date
                and self.players == other.players
                and self.hands == other.hands
            )
        return False

    def __str__(self):
        return f'{self.players} played on {self.date} with the following hands:\n{" ".join(str(h) for h in self.hands)}'

    def getPointsOfPlayer(self, player, ignoreBock=False):
        points = 0
        for hand in self.hands:
            if player in hand.playersWon:
                points += hand.getWinPoints(ignoreBock)
            elif player in hand.playersLost:
                points += hand.getLosePoints(ignoreBock)
        return points

    def getPointsOfTeam(self, players, ignoreBock=False):
        points = 0
        for hand in self.hands:
            if set(players) == set(hand.playersWon):
                points += hand.getWinPoints(ignoreBock)
            elif set(players) == set(hand.playersLost):
                points += hand.getLosePoints(ignoreBock)
        return points

    def getNumberOfHands(self):
        return len(self.hands)


class Hand:
    def __init__(self, id, type, bock, points):
        self.id = id
        self.type = HandTypeStringToEnum(type)  # TODO
        self.bock = bock
        self.points = points
        self.playersWon = list()  # will be added later during XML extraction
        self.playersLost = list()  # will be added later during XML extraction

    def __eq__(self, other):
        if isinstance(other, Hand):
            return (
                self.id == other.id
                and self.type == other.type
                and self.bock == other.bock
                and self.points == other.points
                and self.playersWon == other.playersWon
                and self.playersLost == other.playersLost
            )
        return False

    def __str__(self):
        return f'{self.playersWon} won against {self.playersLost} in a "{self.type}" hand with {self.getWinPoints()} : {self.getLosePoints()} (Bock multiplier: {self.getBockMultiplier(False)})'

    def getWinPoints(self, ignoreBock=False):
        if self.type == HandTypes.NORMAL or self.type == HandTypes.LOSE_SOLO:
            return self.points * self.getBockMultiplier(ignoreBock)
        elif self.type == HandTypes.WIN_SOLO:
            return self.points * 3 * self.getBockMultiplier(ignoreBock)

    def getLosePoints(self, ignoreBock=False):
        if self.type == HandTypes.NORMAL or self.type == HandTypes.WIN_SOLO:
            return -self.points * self.getBockMultiplier(ignoreBock)
        elif self.type == HandTypes.LOSE_SOLO:
            return self.points * (-3) * self.getBockMultiplier(ignoreBock)

    def getPlayers(self):
        return self.playersWon + self.playersLost

    def getBockMultiplier(self, ignoreBock):
        if ignoreBock:
            return 1
        else:
            return 2**self.bock

    def hasPlayerWonSolo(self, player):
        if self.type == HandTypes.WIN_SOLO and player in self.playersWon:
            return True

    def hasPlayerLostSolo(self, player):
        if self.type == HandTypes.LOSE_SOLO and player in self.playersLost:
            return True

    def isSolo(self):
        return self.type == HandTypes.WIN_SOLO or self.type == HandTypes.LOSE_SOLO

    def hasPlayerPlayedSolo(self, player):
        if self.isSolo():
            if (
                len(self.playersWon) == 1
                and player in self.playersWon
                or len(self.playersLost) == 1
                and player in self.playersLost
            ):
                return True
        return False


def HandTypeStringToEnum(typeString):
    if typeString == "normal":
        return HandTypes.NORMAL
    elif typeString == "win_solo":
        return HandTypes.WIN_SOLO
    elif typeString == "lose_solo":
        return HandTypes.LOSE_SOLO
