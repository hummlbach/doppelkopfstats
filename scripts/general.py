from enum import Enum


class StatsTypes(Enum):
    OVERALL = "OVERALL"
    YEAR = "YEAR"
    MONTH = "MONTH"
    NEWEST = "NEWEST"
    RANGED = "RANGED"
    IGNORE_BOCK = "IGNORE_BOCK"
    SOLO_ONLY = "SOLO_ONLY"
    OWN_SOLO_ONLY = "OWN_SOLO_ONLY"
    NO_SOLO = "NO_SOLO"

    def __str__(self):
        return str(self.value)


NORMAL_STATS_LIST = [
    StatsTypes.OVERALL,
    StatsTypes.YEAR,
    StatsTypes.MONTH,
    StatsTypes.NEWEST,
    StatsTypes.RANGED,
]


class HandTypes(Enum):
    NORMAL = 1
    WIN_SOLO = 2
    LOSE_SOLO = 3
