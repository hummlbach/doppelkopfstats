import numpy as np
from enum import Enum

from general import HandTypes

TEAM_PLAYER_DELIMITER = "_w/_"
NUMBER_OF_FREQUENT_OPPONENTS = 5


class SoloType(Enum):
    NO_SOLO = 0
    OWN_SOLO_WON = 1
    OWN_SOLO_LOST = 2
    OTHER_SOLO_WON = 3
    OTHER_SOLO_LOST = 4


class HandFilter:
    def __init__(
        self, dateFilter=(lambda d: True), soloOnly=False, ownSoloOnly=False, noSolo=False
    ):  # TODO: move solo variables to enum or similar
        self.dateFilter = dateFilter
        if noSolo:
            self.soloFilter = lambda h: not h.isSolo()
        elif soloOnly:
            self.soloFilter = lambda h: h.isSolo()
        elif ownSoloOnly:
            self.soloFilter = lambda h: h.isOwnSolo()
        else:
            self.soloFilter = lambda h: True


class Filter:
    def __init__(self, handFilter=HandFilter(), ignoreBock=False):
        self.handFilter = handFilter
        self.ignoreBock = ignoreBock


def setFilter(playersDict, teamsDict, filter=Filter()):
    for key, value in playersDict.items():
        value.filter = filter
        for oppKey, oppValue in value.opponents.items():
            oppValue.filter = filter
    for key, value in teamsDict.items():
        value.filter = filter


class Player:
    def __init__(self, name):
        self.name = name

        self.hands = list()  # list of hands the player has played
        self.filter = Filter()  # for Overall Stats => override to apply other filter
        self.piRatingPerGame = dict()
        self.opponents = dict()  # dict of Player (key is player name)

    def getRelevantHands(self):
        """
        Reduce list of hands to the ones that are relevant for the requested stats, e.g. game-dates or solo-only
        """
        relevantHands = list(
            filter(
                lambda h: self.filter.handFilter.dateFilter(h.gameDate)
                and self.filter.handFilter.soloFilter(h),
                self.hands,
            )
        )
        return relevantHands

    def getListOfGameDates(self):
        gameDates = list()
        for hand in self.getRelevantHands():
            gameDates.append(hand.gameDate)
        return list(set(gameDates))

    def getNumberOfGames(self):
        return len(self.getListOfGameDates())

    def getNumberOfHands(self):
        return len(self.getRelevantHands())

    def getPointsList(self):
        points = list()
        for hand in self.getRelevantHands():
            points.append(hand.getPoints(ignoreBock=self.filter.ignoreBock))
        return points

    def getTotalPoints(self):
        return sum(self.getPointsList())

    def getNumberOfWonHands(self):
        handsWon = 0
        for hand in self.getRelevantHands():
            if hand.basePoints == 0:
                raise Exception("Got 0 points in a hand. Shouldn't this be filtered out?")
            if hand.basePoints > 0:
                handsWon += 1
        return handsWon

    def getHandWinPercentage(self):
        if len(self.getRelevantHands()) == 0:
            return 0
        return self.getNumberOfWonHands() / len(self.getRelevantHands())

    def getHandLosePercentage(self):
        return 1 - self.getHandWinPercentage()

    def getNumberOfSolo(self):
        nSolo = 0
        for hand in self.getRelevantHands():
            if hand.isOwnSolo():
                nSolo += 1
        return nSolo

    def getNumberOfSoloWon(self):
        won = 0
        for hand in self.getRelevantHands():
            if hand.soloType == SoloType.OWN_SOLO_WON:
                won += 1
        return won

    def getNumberOfSoloLost(self):
        lost = 0
        for hand in self.getRelevantHands():
            if hand.soloType == SoloType.OWN_SOLO_LOST:
                lost += 1
        return lost

    def getSoloWinPercentage(self):
        if self.getNumberOfSolo() == 0:
            return 0
        else:
            return self.getNumberOfSoloWon() / self.getNumberOfSolo()

    def getPointsInGame(self):
        pointsInGame = dict()  # key is date, value are points
        gameDates = self.getListOfGameDates()
        for hand in self.getRelevantHands():
            if hand.gameDate not in pointsInGame:
                pointsInGame[hand.gameDate] = 0
            pointsInGame[hand.gameDate] += hand.getPoints(ignoreBock=self.filter.ignoreBock)
        assert len(gameDates) == len(pointsInGame)
        return pointsInGame

    def getNoOfHandsInGame(self):
        handsInGame = dict()  # key is date, value is number of hands
        gameDates = self.getListOfGameDates()
        for hand in self.getRelevantHands():
            if hand.gameDate not in handsInGame:
                handsInGame[hand.gameDate] = 0
            handsInGame[hand.gameDate] += 1
        assert len(gameDates) == len(handsInGame)
        return handsInGame

    def getPointsPerHandInGame(self, isTeam):
        if isTeam:
            minHands = 10
        else:
            minHands = 25
        ppHandInGame = dict()  # key is date, value are points per hand
        pointsInGame = self.getPointsInGame()
        handsInGame = self.getNoOfHandsInGame()
        assert len(pointsInGame) == len(handsInGame)
        # divide by number of hands per game
        totalPoints = 0
        totalHands = 0
        for date in pointsInGame.keys():
            totalPoints += pointsInGame[date]
            totalHands += handsInGame[date]
            if totalHands < minHands:
                continue
            elif totalHands == 0:
                ppHandInGame[date] = 0
            else:
                ppHandInGame[date] = totalPoints / totalHands
        return ppHandInGame

    def getBestWorstGames(self, nGames, isBest):
        ret = dict()
        pointsInGame = self.getPointsInGame()
        sortedKeys = sorted(pointsInGame, key=lambda k: pointsInGame[k], reverse=isBest)
        sortedDict = {k: pointsInGame[k] for k in sortedKeys}

        for i, (gameDate, points) in enumerate(sortedDict.items()):
            # best games can only have postive poistive points and negative games only negative
            if i < nGames and ((points >= 0) == isBest):
                ret[gameDate] = points
            else:
                break
        return ret

    def getAvgPointsPerGame(self):
        if self.getNumberOfGames() == 0:
            return 0
        else:
            return self.getTotalPoints() / self.getNumberOfGames()

    def getAvgPointsPerHand(self):
        if self.getNumberOfHands() == 0:
            return 0
        else:
            return self.getTotalPoints() / self.getNumberOfHands()

    def getHandIdxList(self, games):
        ret = list()
        startHandIdx = 0
        offset = 0
        for game in filter(
            lambda g: self.filter.handFilter.dateFilter(g.date), games
        ):  # only use relevant games
            # print(f"Iterating over game on {game.date}")
            for hand in self.getRelevantHands()[startHandIdx:]:
                if hand.gameDate == game.date:
                    startHandIdx += 1
                    ret.append(hand.index + offset)
            offset += game.getNumberOfHands()
        return ret

    def getHandPointsList(self):
        ret = list()
        for hand in self.getRelevantHands():
            ret.append(hand.getPoints(ignoreBock=self.filter.ignoreBock))
        return ret

    def getPiRatingPerGame(self):
        # use filter to get relevant games
        ret = dict(
            filter(lambda g: self.filter.handFilter.dateFilter(g[0]), self.piRatingPerGame.items())
        )  # TODO: can we use filter member here?
        return ret

    def getCurrentPiRating(self):
        list = [*self.getPiRatingPerGame().values()]
        return list[-1]  # last element is current rating

    def getMostFrequentOppNames(self, returnIsString):
        sortedKeys = sorted(
            self.opponents, key=lambda k: self.opponents[k].getNumberOfHands(), reverse=True
        )
        sortedOpponentKeys = {k: self.opponents[k] for k in sortedKeys}.keys()
        if returnIsString:
            return ", ".join(sortedOpponentKeys)
        else:
            return sortedOpponentKeys

    def getAvgPiRatingOfFrequentOpponents(self, opponents):
        sortedOpponents = self.getMostFrequentOppNames(returnIsString=False)
        piRatings = list()
        for opp in sortedOpponents:
            if self.opponents[opp].getNumberOfHands() == 0:  # skip players without hands
                continue
            if len(piRatings) >= NUMBER_OF_FREQUENT_OPPONENTS:
                break
            piRatings.append(opponents[opp].getCurrentPiRating())
        return sum(piRatings) / len(piRatings)


class Team(Player):
    def __init__(self, playerA, playerB):
        self.players = sorted([playerA, playerB])
        super().__init__(createTeamName([playerA, playerB]))

    def isPlayerInTeam(self, player):
        if player in self.players:
            return True
        else:
            return False

    def getPartnerOfPlayer(self, player):
        players = self.players.copy()
        players.remove(player)
        if len(players) != 1:
            raise Exception(f"Cannot determine partner of {player} in {self.players}")
        return players[0]


class PlayerHand:
    def __init__(self, gameDate, handIdx, handType: HandTypes, points, bockMult):
        self.gameDate = gameDate
        self.index = handIdx
        self.bockMult = bockMult
        self.basePoints = points  # without bock multiplier but WITH solo
        if handType == HandTypes.WIN_SOLO:
            if self.basePoints > 0:
                self.soloType = SoloType.OWN_SOLO_WON
            else:
                self.soloType = SoloType.OTHER_SOLO_WON
        elif handType == HandTypes.LOSE_SOLO:
            if self.basePoints < 0:
                self.soloType = SoloType.OWN_SOLO_LOST
            else:
                self.soloType = SoloType.OTHER_SOLO_LOST
        elif handType == HandTypes.NORMAL:
            self.soloType = SoloType.NO_SOLO
        else:
            raise Exception(f"Unknown hand type {handType}")

    def getPoints(self, ignoreBock):
        return self.basePoints * self.getBockMultiplier(ignoreBock)

    def isBock(self):
        if self.bockMult > 1:
            return True
        else:
            return False

    def getBockMultiplier(self, ignoreBock):
        if ignoreBock:
            return 1
        else:
            return 2**self.bockMult

    def isOwnSolo(self):
        if self.soloType == SoloType.OWN_SOLO_WON or self.soloType == SoloType.OWN_SOLO_LOST:
            return True
        else:
            return False

    def isSolo(self):
        if self.soloType != SoloType.NO_SOLO:
            return True
        else:
            return False


def createTeamName(playerList):  # TODO: Move to other file
    if len(playerList) != 2:
        raise Exception(
            f"createTeamName() expects exactly two player names, but {len(playerList)} were provided"
        )
    playerListSorted = sorted(playerList)
    return f"{playerListSorted[0]}{TEAM_PLAYER_DELIMITER}{playerListSorted[1]}"


def getPlayersInTeam(teamName):
    if not isTeamName(teamName):
        raise Exception("Provided team name is no team!")
    return teamName.split(TEAM_PLAYER_DELIMITER)


def isTeamName(name):
    return TEAM_PLAYER_DELIMITER in name
