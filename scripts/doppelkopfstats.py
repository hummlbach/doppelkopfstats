#!/usr/bin/python3

"""Generate statistics from game data exported by 'Doppelkopf' app."""

# import collections
import datetime
import pathlib
import re
import sys
import xml.etree.ElementTree as ET
import yaml

import jinja2
import matplotlib.pyplot as plt

# custom
from game import Game, Hand
from highscore import *

# from stats import HandStats, PlayerStats, TeamStats, createTeamName
from player import Player, PlayerHand, Team, createTeamName
from plot import PlotData, plotHighscoreAsBar, PlotTypes
from general import StatsTypes, HandTypes, NORMAL_STATS_LIST
from pi_rating import *

# TODO: use logging library and add verbose argument to set if we want to have debug output

# global variables
plotGuards = {
    StatsTypes.OVERALL: True,
    StatsTypes.YEAR: True,
    StatsTypes.MONTH: True,
    StatsTypes.NEWEST: True,
    StatsTypes.RANGED: True,
}
processHighscores = True
printStats = True


def filenameToDate(path):
    """
    Parse game result xml filenames (like 04_12_2019_19_23_43_dokoSavedGame.xml) to get the date.

    :param str path: Filename or path to parse for date
    :return: datetime element
    """
    match = re.search(r"(\d{2})_(\d{2})_(\d{4})", str(path))
    return datetime.date(int(match.group(3)), int(match.group(2)), int(match.group(1)))


def parseHands(handsXML) -> list[Hand]:
    """
    Extract information of each hand (ID, Type, Points, Bock).

    :param ET handsXML: Hands element from XML (Element tree object)
    :return: List of objects of type "Hand"
    """
    hands = list()
    for r in handsXML:
        roundDict = {}
        # loop through all rounds and store the info ("tag") for each round
        # (tags to store: ID, Type, Points, (Bock))
        for roundInfo in r:
            # add empty list for this tag in case its not already present
            if roundInfo.tag not in roundDict:
                roundDict[roundInfo.tag] = []
            # add element to list
            roundDict[roundInfo.tag] = roundInfo.text
        hands.append(
            Hand(
                int(roundDict["RoundID"]),
                roundDict["RoundType"],
                int(roundDict["RoundBockCount"]),
                int(roundDict["RoundPoints"]),
            )
        )

    return hands


def parsePlayers(playersXML, hands: list[Hand]) -> list[Hand]:
    """
    Extract information for each player and add it to Hand objects.

    :param ET playersXML: Players element from XML (Element tree object)
    :param object hands: Object of class "Hand" to store the information per player for each round
    :return: Hand object extended with new information
    """
    for player in playersXML:
        playerName = player.find("name").text
        # skip "empty" player
        if playerName == None:
            continue

        # extract points per round
        totalPoints = 0  # store total points to check if data is correct below
        for i, e in enumerate(player.find("PointsHistory")):
            currentHand = hands[i]
            for pointsInfo in e:
                if pointsInfo.tag == "PointAtRound":
                    points = int(float(pointsInfo.text))
                    if points == 0:  # player was suspended in that hand
                        continue
                    totalPoints = totalPoints + points
                    if currentHand.getWinPoints() == points:
                        currentHand.playersWon.append(playerName)
                    elif currentHand.getLosePoints() == points:
                        currentHand.playersLost.append(playerName)
                    else:
                        raise Exception(
                            f"Unexpected points for player {playerName}. It neither does match points for winning nor for losing party of that hand."
                        )

        if int(float(player.find("points").text)) != totalPoints:
            raise Exception(
                f"Sum of points for each hand ({totalPoints}) and total number of points ({player.find('points').text}) does not add up for player {playerName}! Hint: Was the file manually edited?"
            )

    # return hands object with newly added information
    return hands


def createStatsText(
    playersDict: dict[str, Player], teamsDict: dict[str, Team], dataType: StatsTypes
):
    """
    Convert content of collected data (single and team) into human readable statistics text.

    :return: Summary in human-readable text format
    """
    outputString = ""
    validPlayers = 0

    # sort by points scored
    for playerName, player in playersDict.items():
        # skip players with no hands played
        if len(player.getRelevantHands()) == 0:
            continue
        else:
            validPlayers += 1
        # collect content to write/print
        emoji = ""
        if validPlayers == 1:
            emoji = "🥇"
        elif validPlayers == 2:
            emoji = "🥈"
        elif validPlayers == 3:
            emoji = "🥉"
        outputString += f"\n### {emoji} {validPlayers}. {playerName} {emoji} ###\n"
        outputString += f"Points: {player.getTotalPoints()} "
        outputString += f"({player.getAvgPointsPerGame():3.2f} per game, {player.getAvgPointsPerHand():3.2f} per hand)\n"
        if dataType in NORMAL_STATS_LIST:
            outputString += (
                f"PiRating: {player.getCurrentPiRating():.2f} "
                f"(max.: {max(player.getPiRatingPerGame().values()):.2f}, "
                f"min.: {min(player.getPiRatingPerGame().values()):.2f})\n"
                f"Expected points vs. avg. players: {calculateExpectedPoints(player.getCurrentPiRating(), 0):.2f} (vs. most frequent players: {calculateExpectedPoints(player.getCurrentPiRating(), player.getAvgPiRatingOfFrequentOpponents(playersDict)):.2f})\n"
            )
        outputString += (
            f"Played: {player.getNumberOfHands()} hands "
            f"(Won: {(player.getHandWinPercentage() * 100):3.2f}%, "
            f"Lost: {((player.getHandLosePercentage()) * 100):3.2f}%) in "
            f"{player.getNumberOfGames()} games\n"
        )
        outputString += (
            f"Solos: {player.getNumberOfSoloWon()} Win(s) ({(player.getSoloWinPercentage() * 100):3.2f}%), "
            f"{player.getNumberOfSoloLost()} Loss(es) "
            f"({player.getNumberOfSolo()} total)\n"
        )

        # get best and worst games
        if dataType not in [StatsTypes.SOLO_ONLY, StatsTypes.OWN_SOLO_ONLY]:
            noOfBestWorstGames = 4
            outputString += f"Best Games: {createTextBestWorstGames(player.getBestWorstGames(noOfBestWorstGames, True))}\n"
            outputString += f"Worst Games: {createTextBestWorstGames(player.getBestWorstGames(noOfBestWorstGames, False))}\n"

            # find team entry in dict
            for teamname, opponent in teamsDict.items():
                if opponent.isPlayerInTeam(playerName) and len(opponent.getRelevantHands()) > 0:
                    outputString += (
                        f"With {opponent.getPartnerOfPlayer(playerName)}:\t "
                        f"{(opponent.getTotalPoints()):4} points in "
                        f"{(opponent.getNumberOfHands()):4} (normal) hands "
                        f"(avg. {(opponent.getAvgPointsPerHand()): .2f} points), "
                        f"won {(opponent.getHandWinPercentage() * 100):6.2f}% and "
                        f"lost {((opponent.getHandLosePercentage()) * 100):6.2f}%\n"
                    )
            outputString += "===================================================================================================\n"
            for opponentName, opponent in player.opponents.items():
                if len(opponent.getRelevantHands()) == 0:
                    continue
                outputString += (
                    f"vs. {opponentName}:\t "
                    f"{(opponent.getTotalPoints()):4} points in "
                    f"{(opponent.getNumberOfHands()):4} (normal) hands "
                    f"(avg. {(opponent.getAvgPointsPerHand()): .2f} points), "
                    f"won {(opponent.getHandWinPercentage() * 100):6.2f}% and "
                    f"lost {((opponent.getHandLosePercentage()) * 100):6.2f}%\n"
                )

    if printStats:  # FIXME: when logging lib is used
        # print collected content / write to file
        print(outputString, end="")

    return outputString


def createTextBestWorstGames(gameDict):
    outputString = ""
    for i, (game, points) in enumerate(gameDict.items()):
        if i != 0:
            outputString += ", "
        outputString += f"{points:3} ({game:%d-%b-%Y})"
    return outputString


def plotPoints(plotDataList: list[PlotData], filename, title, setXTicks=False):
    """
    Create and store a plot.

    :param list plotDataList:
    :param Path filename: Path and filename of the plot without extension
    :param str title:
    :param bool setXTicks:
    :return bool: Data was plotted
    """

    if len(plotDataList) == 0:
        print(f"No data to plot for filename={filename}, title={title}")
        return False

    fig = plt.figure(figsize=[16, 10])
    plt.grid(linestyle="dotted")
    # store handles to reorder them later and to add rank
    legendHandles = []

    for data in plotDataList:
        legendHandles.append(data.plot(setXTicks))

    plt.ylabel(plotDataList[0].type.value)
    plt.title(title)
    # reverse order of labels because 1st player was plotted last
    legendHandles = list(reversed(legendHandles))
    for i, lh in enumerate(legendHandles):
        player = lh.get_label()
        label = f"{player} ({i + 1}.)"
        lh.set_label(label)
    # set edited legend
    plt.legend(handles=legendHandles)
    plt.savefig(filename.with_suffix(".png"), bbox_inches="tight")
    # close opened figure to fix warning:
    # "RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are
    # retained until explicitly closed and may consume too much memory."
    plt.close(fig)

    return True


def calculateHighscoresForDateType(highscoreDict, playersOrTeamsDict, dateType, isTeamStats):
    # read highscore infos from YAML file
    with open("scripts/highscoreInfos.yaml", "r") as yamlFile:
        highscoreInfos = yaml.safe_load(yamlFile)

    minGames = dict()
    minHands = dict()

    if isTeamStats:
        minGames[StatsTypes.OVERALL] = 5
        minGames[StatsTypes.YEAR] = 3
        minGames[StatsTypes.MONTH] = 1
        minHands[StatsTypes.OVERALL] = 25
        minHands[StatsTypes.YEAR] = 15
        minHands[StatsTypes.MONTH] = 8
    else:
        minGames[StatsTypes.OVERALL] = 20
        minGames[StatsTypes.YEAR] = 7
        minGames[StatsTypes.MONTH] = 2
        minHands[StatsTypes.OVERALL] = 100
        minHands[StatsTypes.YEAR] = 35
        minHands[StatsTypes.MONTH] = 15

    # loop through all highscores defined in YAML file
    for key, value in highscoreInfos.items():
        # FIXME: (looks ugly) only process highscores matching stats type (indiv vs team)
        if not (
            (value["isTeamStat"] and isTeamStats) or (value["isIndivStat"] and not isTeamStats)
        ):
            continue
        if key not in highscoreDict:
            highscoreDict[key] = Highscore(
                key, value["title"], value["ylabel"], value["dataIsFloat"]
            )
        if not highscoreDict[key].isInitialized[dateType]:
            if value[str(dateType)]["isRelevant"] == False:
                continue
            highscoreDict[key].addDateAndEntries(dateType, value[str(dateType)]["nEntries"])

        playerOrTeam: Player
        for playerName, playerOrTeam in playersOrTeamsDict.items():
            # check game and hands filter
            if (
                playerOrTeam.getNumberOfHands() < minHands[dateType]
                or playerOrTeam.getNumberOfGames() < minGames[dateType]
            ):
                continue
            # get date extension (e.g. "Sep 2021" for month)
            date = playerOrTeam.getListOfGameDates()[0]
            dateExtension = ""
            if dateType == StatsTypes.YEAR:
                dateExtension = f"{date.year}"
            elif dateType == StatsTypes.MONTH:
                dateExtension = f"{date:%b %Y}"
            # process highscores
            if key == "Points":
                highscoreDict[key].addEntryToDate(
                    dateType, playerOrTeam.name, playerOrTeam.getTotalPoints(), dateExtension
                )
            elif key == "Pointspg":
                highscoreDict[key].addEntryToDate(
                    dateType, playerOrTeam.name, playerOrTeam.getAvgPointsPerGame(), dateExtension
                )
            elif key == "Pointsph":
                highscoreDict[key].addEntryToDate(
                    dateType, playerOrTeam.name, playerOrTeam.getAvgPointsPerHand(), dateExtension
                )
            elif key == "Solos":
                highscoreDict[key].addEntryToDate(
                    dateType, playerOrTeam.name, playerOrTeam.getNumberOfSolo(), dateExtension
                )
            elif key == "SoloW":
                highscoreDict[key].addEntryToDate(
                    dateType, playerOrTeam.name, playerOrTeam.getNumberOfSoloWon(), dateExtension
                )
            elif key == "SoloWRate":
                highscoreDict[key].addEntryToDate(
                    dateType, playerOrTeam.name, playerOrTeam.getSoloWinPercentage(), dateExtension
                )
            elif key == "Hands":
                highscoreDict[key].addEntryToDate(
                    dateType, playerOrTeam.name, playerOrTeam.getNumberOfHands(), dateExtension
                )
            elif key == "HandsW":
                highscoreDict[key].addEntryToDate(
                    dateType, playerOrTeam.name, playerOrTeam.getNumberOfWonHands(), dateExtension
                )
            elif key == "HandsWRate":
                highscoreDict[key].addEntryToDate(
                    dateType, playerOrTeam.name, playerOrTeam.getHandWinPercentage(), dateExtension
                )
            elif key == "PointsInGame":
                for points in playerOrTeam.getPointsInGame().values():
                    highscoreDict[key].addEntryToDate(
                        dateType, playerOrTeam.name, points, dateExtension
                    )
            elif key == "PointsInHand":
                for points in playerOrTeam.getPointsList():
                    highscoreDict[key].addEntryToDate(
                        dateType, playerOrTeam.name, points, dateExtension
                    )

    return highscoreDict


def plotHighscores(highscoreDict, outpath, isTeamStats):
    filenames = list()
    # read highscore infos from YAML file
    with open("scripts/highscoreInfos.yaml", "r") as yamlFile:
        highscoreInfos = yaml.safe_load(yamlFile)

    # loop through all highscores defined in YAML file
    for key, value in highscoreInfos.items():
        # FIXME: (looks ugly) only process highscores matching stats type (indiv vs team)
        if not (
            (value["isTeamStat"] and isTeamStats) or (value["isIndivStat"] and not isTeamStats)
        ):
            continue
        highscore = highscoreDict[key]
        # process all types (overall, year, month)
        for dateType in [StatsTypes.OVERALL, StatsTypes.YEAR, StatsTypes.MONTH]:
            if value[str(dateType)]["isRelevant"] == False:
                continue

        filename = f"Team_{key}" if isTeamStats else f"{key}"
        plotHighscoreAsBar(highscore, outpath / filename)
        filenames.append(filename)

    return filenames


def processGames(games):
    """
    Convert list games passed to this function into single player and team stats objects stored in dictionaries.
    """

    playersDict = dict()  # to easier check if player already exists
    teamsDict = dict()

    def addPlayerOrTeamIfNotExisting(statsDict: dict, playerA: str, playerB: str = None):
        # Single Player
        if playerB == None:
            if playerA not in statsDict:
                statsDict[playerA] = Player(playerA)
        # Team
        else:
            teamName = createTeamName([playerA, playerB])
            if teamName not in statsDict:
                statsDict[teamName] = Team(playerA, playerB)
        return statsDict

    # iterate over hands
    for game in games:
        for handIdx, hand in enumerate(game.hands):
            # skip hands with 0 points since we don't know who has acutally played # NOTE: has to be adapted after improving Android App
            if hand.getWinPoints() == 0:
                continue
            # Single Player
            for player in hand.playersWon:
                playersDict = addPlayerOrTeamIfNotExisting(playersDict, player)
                # get stats of Hand to create PlayerHand object
                playersDict[player].hands.append(
                    PlayerHand(
                        game.date, handIdx, hand.type, hand.getWinPoints(ignoreBock=True), hand.bock
                    )
                )
                # for normal hands we track opponent stats
                if hand.type == HandTypes.NORMAL:
                    for opponent in hand.playersLost:
                        playersDict[player].opponents = addPlayerOrTeamIfNotExisting(
                            playersDict[player].opponents, opponent
                        )
                        playersDict[player].opponents[opponent].hands.append(
                            PlayerHand(
                                game.date,
                                handIdx,
                                hand.type,
                                hand.getWinPoints(ignoreBock=True),
                                hand.bock,
                            )
                        )
            for player in hand.playersLost:
                playersDict = addPlayerOrTeamIfNotExisting(playersDict, player)
                # get stats of Hand to create PlayerHand object
                playersDict[player].hands.append(
                    PlayerHand(
                        game.date,
                        handIdx,
                        hand.type,
                        hand.getLosePoints(ignoreBock=True),
                        hand.bock,
                    )
                )
                # for normal hands we track opponent stats
                if hand.type == HandTypes.NORMAL:
                    for opponent in hand.playersWon:
                        playersDict[player].opponents = addPlayerOrTeamIfNotExisting(
                            playersDict[player].opponents, opponent
                        )
                        playersDict[player].opponents[opponent].hands.append(
                            PlayerHand(
                                game.date,
                                handIdx,
                                hand.type,
                                hand.getLosePoints(ignoreBock=True),
                                hand.bock,
                            )
                        )
            # Team (not for Soli)
            if hand.type == HandTypes.NORMAL:
                # Won
                teamsDict = addPlayerOrTeamIfNotExisting(
                    teamsDict, hand.playersWon[0], hand.playersWon[1]
                )
                winTeamName = createTeamName(hand.playersWon)
                teamsDict[winTeamName].hands.append(
                    PlayerHand(
                        game.date, handIdx, hand.type, hand.getWinPoints(ignoreBock=True), hand.bock
                    )
                )
                # Lost
                teamsDict = addPlayerOrTeamIfNotExisting(
                    teamsDict, hand.playersLost[0], hand.playersLost[1]
                )
                loseTeamName = createTeamName(hand.playersLost)
                teamsDict[loseTeamName].hands.append(
                    PlayerHand(
                        game.date,
                        handIdx,
                        hand.type,
                        hand.getLosePoints(ignoreBock=True),
                        hand.bock,
                    )
                )

        # Pi Rating
        calculateRatingChangesForSingleGame(game, playersDict)
    return playersDict, teamsDict


def calcPlotData(playersDict, games, xLabel, plotType, isTeam):
    """ """
    dataList = list()
    player: Player
    for name, player in playersDict.items():
        tmpData = PlotData(name, xLabel, plotType)
        if plotType == PlotTypes.Points:
            for date, points in player.getPointsInGame().items():
                tmpData.x.add(date)  # FIXME: set() or list()?
                tmpData.y.append(points)
        elif plotType == PlotTypes.Hands:
            tmpData.x = player.getHandIdxList(games)
            tmpData.y = player.getHandPointsList()
        elif plotType == PlotTypes.PiRating:
            for date, points in player.getPointsInGame().items():
                tmpData.x = [*player.getPiRatingPerGame().keys()]
                tmpData.y = [*player.getPiRatingPerGame().values()]
            tmpData.setAnnotationDecimalPlaces(2)
        elif plotType == PlotTypes.ppHands:
            for date, ppHand in player.getPointsPerHandInGame(isTeam=isTeam).items():
                tmpData.x.add(date)  # FIXME: set() or list()?
                tmpData.y.append(ppHand)
            tmpData.setAnnotationDecimalPlaces(2)
        else:
            raise Exception(f'Unknown plot type "{plotType}"')
        # workaround to be able to get the plot order (Note: Will be done again in PlotData.plot())
        if plotType in [PlotTypes.PiRating, PlotTypes.ppHands]:
            tmpData.y_cumulative = tmpData.y
        else:
            tmpData.calcCumuluativeSumY()
        if len(tmpData.x) != len(tmpData.y):
            raise Exception(
                f"Length of x ({len(tmpData.x)}) and y ({len(tmpData.y)}) data for plot of type {plotType} is unequal!"
            )
        elif len(tmpData.x) == 0 or (tmpData.y) == 0:
            continue
        dataList.append(tmpData)
    # sort from lowest to highest
    dataList.sort(key=lambda x: x.y_cumulative[-1])
    return dataList


def createStatsOutput(
    playersDict,
    teamsDict,
    date,
    games,
    dataType: StatsTypes,
    htmlRows,
    plotGuards,
    plotpath,
    individualName=None,
    addNewHeading=True,
):
    """
    Process the stats of a specific period.

    :param list games: List of objects of class "Game" which holds all data
    :param datatime date: Datetime object which is used to filter for relevant games (can also be a dict to filter for a time
        period (start, end))
    :param string dataType: The data-type to process (e.g. OVERALL, YEAR, ..)
    :param list htmlRows: Used for appending the calculated data to the HTML output
    :param dict plotGuards: Guards that are used to increase processing time of the script by skipping steps
    :param path plotpath: Path used for storing the plots
    :param string individualName: Define a specific name (used for filename and in HTML); useful for types like RANGE that have
        custom periods.
    :param boolean addNewHeading: Add heading in HTML before printing data. Is "True" by default but can be deactivated in case
        a group of elements is processed that only need a single header.
    """
    # quit function if nothing to do
    if len(playersDict) == 0:
        return

    HTMLdetailsOpen = False
    if dataType == StatsTypes.OVERALL:
        HTMLdetailsOpen = True
        plotPrefix = "Overall"
        HTMLsummary = "Overall statistics"
    elif dataType == StatsTypes.YEAR:
        plotPrefix = str(date.year)
        HTMLsummary = f"Statistics in {date.year}"
    elif dataType == StatsTypes.MONTH:
        plotPrefix = f"{date:%B}"
        HTMLsummary = f"Statistics in {date:%b %Y}"
    elif dataType == StatsTypes.NEWEST:
        plotPrefix = f"{date.isoformat()}"
        HTMLsummary = plotPrefix
    elif dataType == StatsTypes.RANGED:
        plotPrefix = individualName
        HTMLsummary = f"{plotPrefix} statistics"
    elif dataType == StatsTypes.IGNORE_BOCK:
        plotPrefix = "Overall No Bock"
        HTMLsummary = "Overall statistics w\o bock multiplier"
    elif dataType == StatsTypes.SOLO_ONLY:
        plotPrefix = "Overall Solo only"
        HTMLsummary = "Overall statistics in Solo hands"
    elif dataType == StatsTypes.OWN_SOLO_ONLY:
        plotPrefix = "Overall Own Solo only"
        HTMLsummary = "Overall statistics in own Solo hands"
    elif dataType == StatsTypes.NO_SOLO:
        plotPrefix = "Overall No Solo"
        HTMLsummary = "Overall statistics w\o Solo hands"
    else:
        raise Exception(f'Unknown data type "{dataType}"')

    if dataType not in plotGuards or plotGuards[dataType]:
        # sort dicts by most points
        sortedKeys = sorted(
            playersDict, key=lambda k: playersDict[k].getTotalPoints(), reverse=True
        )
        playersDict = {k: playersDict[k] for k in sortedKeys}
        sortedKeys = sorted(teamsDict, key=lambda k: teamsDict[k].getTotalPoints(), reverse=True)
        teamsDict = {k: teamsDict[k] for k in sortedKeys}
        for player, stats in playersDict.items():
            sortedKeys = sorted(
                stats.opponents, key=lambda k: stats.opponents[k].getTotalPoints(), reverse=True
            )
            stats.opponents = {k: stats.opponents[k] for k in sortedKeys}

        print(
            "\n\n###################################\n"
            f"##### {plotPrefix} STATISTICS #####\n"
            "###################################"
        )
        statsText = createStatsText(playersDict, teamsDict, dataType)

        # collect available plots for each type (hands-plot is the only one available for all)
        relpath = plotpath.relative_to("build")
        plotsForHTML = list()

        # SINGLE player
        if dataType not in [StatsTypes.NEWEST]:
            # Points plot over time (single player)
            plotDataList = calcPlotData(
                playersDict, games, xLabel="Date", plotType=PlotTypes.Points, isTeam=False
            )
            if plotPoints(
                plotDataList,
                plotpath / f"{plotPrefix}_Points",
                plotPrefix + " - Points over time",
                setXTicks=True,
            ):
                plotsForHTML.append(relpath / f"{plotPrefix}_Points")

        # Hands plot single player
        plotDataList = calcPlotData(
            playersDict, games, xLabel="Hand", plotType=PlotTypes.Hands, isTeam=False
        )
        if plotPoints(
            plotDataList,
            plotpath / f"{plotPrefix}_Hands",
            plotPrefix + " - Points over hands",
        ):
            plotsForHTML.append(relpath / f"{plotPrefix}_Hands")

        if dataType in [StatsTypes.OVERALL, StatsTypes.YEAR, StatsTypes.MONTH, StatsTypes.RANGED]:
            # Pi Rating Plot
            plotDataList = calcPlotData(
                playersDict, games, xLabel="Date", plotType=PlotTypes.PiRating, isTeam=False
            )
            if plotPoints(
                plotDataList,
                plotpath / f"{plotPrefix}_PiRating",
                plotPrefix + " - Pi-Rating over time",
                setXTicks=True,
            ):
                plotsForHTML.append(relpath / f"{plotPrefix}_PiRating")

        # points per hand
        if dataType in [StatsTypes.OVERALL, StatsTypes.YEAR]:
            plotDataList = calcPlotData(
                playersDict, games, xLabel="Date", plotType=PlotTypes.ppHands, isTeam=False
            )
            if plotPoints(
                plotDataList,
                plotpath / f"{plotPrefix}_ppHands",
                plotPrefix + " - Points per hand",
            ):
                plotsForHTML.append(relpath / f"{plotPrefix}_ppHands")

        # TEAMS
        if dataType not in [StatsTypes.SOLO_ONLY, StatsTypes.OWN_SOLO_ONLY, StatsTypes.NO_SOLO]:
            if dataType not in [StatsTypes.NEWEST]:
                # Hands plot over time
                plotDataList = calcPlotData(
                    teamsDict, games, xLabel="Date", plotType=PlotTypes.Points, isTeam=True
                )
                # 10 best teams (by points)
                if plotPoints(
                    plotDataList[-10:],
                    plotpath / f"Team_{plotPrefix}_Points",
                    "Teams " + plotPrefix + " - Points over time",
                    setXTicks=True,
                ):
                    plotsForHTML.append(relpath / f"Team_{plotPrefix}_Points")

            # Hands plot teams
            plotDataList = calcPlotData(
                teamsDict, games, xLabel="Hand", plotType=PlotTypes.Hands, isTeam=True
            )
            if plotPoints(
                plotDataList[-10:],
                plotpath / f"Team_{plotPrefix}_Hands",
                "Teams " + plotPrefix + " - Points over hands",
            ):
                plotsForHTML.append(relpath / f"Team_{plotPrefix}_Hands")

            # points per hand
            if dataType in [StatsTypes.OVERALL, StatsTypes.YEAR]:
                plotDataList = calcPlotData(
                    teamsDict, games, xLabel="Date", plotType=PlotTypes.ppHands, isTeam=True
                )
                if plotPoints(
                    plotDataList[-10:],
                    plotpath / f"Team_{plotPrefix}_ppHands",
                    "Teams " + plotPrefix + " - Points per hand",
                ):
                    plotsForHTML.append(relpath / f"Team_{plotPrefix}_ppHands")

        # append data to HTML file
        htmlRows.append(
            {
                "open": HTMLdetailsOpen,
                "summary": HTMLsummary,
                "stats": statsText,
                "plots": plotsForHTML,
            }
        )
        # sub headers are relevant for all types but monthly stats
        if dataType != StatsTypes.MONTH:
            # newest games need only one header for several entries; check for first element
            if addNewHeading:
                if dataType != StatsTypes.NEWEST:
                    htmlRows[-1]["heading"] = plotPrefix
                else:
                    htmlRows[-1]["heading"] = "Newest games"


def parseXML(file):
    """ """
    # date to extract
    date = ""
    hands = list()

    with open(file, "rt") as f:
        # get data from XML
        tree = ET.parse(f)
        root = tree.getroot()
        for child in root:
            if child.tag == "GamSettings":
                for setting in child:
                    # get date of creation
                    if setting.tag == "CreateDate":
                        match = re.search(r"(\d{4})-(\d{2})-(\d{2})", setting.text)
                        date = datetime.date(
                            int(match.group(1)), int(match.group(2)), int(match.group(3))
                        )
            # extract round info and store them in game object
            if child.tag == "Rounds":
                hands = parseHands(child)
            elif child.tag == "Players":
                hands = parsePlayers(child, hands)

    # get all players that participated in that game
    players = set()  # we don't want to have duplicates
    for hand in hands:
        players.update(hand.getPlayers())

    return Game(date, list(players), hands)


def main():
    """
    Parse game result files and create statistics.
    """
    if len(sys.argv) < 2:
        raise Exception("Please provide path to result files folder!")
    path = sys.argv[1]
    outpath = pathlib.Path("build")
    outpath.mkdir(parents=True, exist_ok=True)
    # TODO: remove existing files in build folder

    files = sorted(pathlib.Path(path).glob("*.xml"), key=filenameToDate)

    allGames = list()
    yearsToPrint = set()
    for file in files:
        print(f"*** Processing {file} ***")
        # store year of game in a set to know which yearly-stats need to be printed
        newGame = parseXML(file)
        allGames.append(newGame)
        yearsToPrint.add(newGame.date.year)

    # for HTML output-file
    HTMLRows = []
    # highscores stuff
    playerHighscoreDict = dict()
    teamHighscoreDict = dict()
    highscoreFolder = outpath / "highscores"
    highscoreFolder.mkdir(parents=True, exist_ok=True)
    highscoreFilenames = list()

    playersDict, teamsDict = processGames(allGames)

    # OVERALL Stats
    setFilter(playersDict, teamsDict, Filter())  # use default hand filter (=no filtering at all)
    createStatsOutput(
        playersDict, teamsDict, None, allGames, StatsTypes.OVERALL, HTMLRows, plotGuards, outpath
    )
    playerHighscoreDict = calculateHighscoresForDateType(
        playerHighscoreDict, playersDict, StatsTypes.OVERALL, False
    )
    teamHighscoreDict = calculateHighscoresForDateType(
        teamHighscoreDict, teamsDict, StatsTypes.OVERALL, True
    )

    # OVERALL without BockCount
    setFilter(playersDict, teamsDict, Filter(ignoreBock=True))
    createStatsOutput(
        playersDict,
        teamsDict,
        None,
        allGames,
        StatsTypes.IGNORE_BOCK,
        HTMLRows,
        plotGuards,
        outpath,
        addNewHeading=False,
    )

    # OVERALL Solo only
    setFilter(playersDict, teamsDict, Filter(handFilter=HandFilter(soloOnly=True)))
    createStatsOutput(
        playersDict,
        teamsDict,
        None,
        allGames,
        StatsTypes.SOLO_ONLY,
        HTMLRows,
        plotGuards,
        outpath,
        addNewHeading=False,
    )

    # OVERALL Own Solo only
    setFilter(playersDict, teamsDict, Filter(handFilter=HandFilter(ownSoloOnly=True)))
    createStatsOutput(
        playersDict,
        teamsDict,
        None,
        allGames,
        StatsTypes.OWN_SOLO_ONLY,
        HTMLRows,
        plotGuards,
        outpath,
        addNewHeading=False,
    )

    # OVERALL Own Solo only
    setFilter(playersDict, teamsDict, Filter(handFilter=HandFilter(noSolo=True)))
    createStatsOutput(
        playersDict,
        teamsDict,
        None,
        allGames,
        StatsTypes.NO_SOLO,
        HTMLRows,
        plotGuards,
        outpath,
        addNewHeading=False,
    )

    # NEWEST games
    newestGamesToPlot = 5
    relevantGames = allGames[-newestGamesToPlot:]
    relevantGames.reverse()  # from new to old
    lastGame = relevantGames[0]
    firstGame = relevantGames[-1]
    dateFilter = lambda d: d >= firstGame.date and d <= lastGame.date
    setFilter(playersDict, teamsDict, Filter(handFilter=HandFilter(dateFilter=dateFilter)))
    createStatsOutput(
        playersDict,
        teamsDict,
        None,
        allGames,
        StatsTypes.RANGED,
        HTMLRows,
        plotGuards,
        outpath,
        "Newest games",
    )

    for game in relevantGames:
        # createStatsOutput([game], game.date, StatsTypes.NEWEST, rows, plotGuards, outpath, SpecialStats.NORMAL, None, False)
        dateFilter = lambda d: d == game.date
        setFilter(playersDict, teamsDict, Filter(handFilter=HandFilter(dateFilter=dateFilter)))
        createStatsOutput(
            playersDict,
            teamsDict,
            game.date,
            allGames,
            StatsTypes.NEWEST,
            HTMLRows,
            plotGuards,
            outpath,
            None,
            False,
        )

    # YEARLY stats
    for year in reversed(list(yearsToPrint)):  # new to old
        date = datetime.date(year, 1, 1)
        dateFilter = lambda d: d.year == year
        setFilter(playersDict, teamsDict, Filter(handFilter=HandFilter(dateFilter=dateFilter)))
        createStatsOutput(
            playersDict, teamsDict, date, allGames, StatsTypes.YEAR, HTMLRows, plotGuards, outpath
        )
        playerHighscoreDict = calculateHighscoresForDateType(
            playerHighscoreDict, playersDict, StatsTypes.YEAR, False
        )
        teamHighscoreDict = calculateHighscoresForDateType(
            teamHighscoreDict, teamsDict, StatsTypes.YEAR, True
        )

        # MONTHLY stats
        for month in range(12, 0, -1):
            date = datetime.date(year, month, 1)
            dateFilter = lambda d: d.year == year and d.month == month
            # check if there acutally was at least one game in that month
            if len(list(filter(lambda g: dateFilter(g.date), allGames))) == 0:
                print(f"No Games in {date}")
                continue
            # create subfolder for year of month to plot (if not already existing)
            yearfolder = outpath / str(date.year)
            yearfolder.mkdir(parents=True, exist_ok=True)
            setFilter(playersDict, teamsDict, Filter(handFilter=HandFilter(dateFilter=dateFilter)))

            createStatsOutput(
                playersDict,
                teamsDict,
                date,
                allGames,
                StatsTypes.MONTH,
                HTMLRows,
                plotGuards,
                yearfolder,
            )
            playerHighscoreDict = calculateHighscoresForDateType(
                playerHighscoreDict, playersDict, StatsTypes.MONTH, False
            )
            teamHighscoreDict = calculateHighscoresForDateType(
                teamHighscoreDict, teamsDict, StatsTypes.MONTH, True
            )

    # highscore calculation
    highscoreRows = {
        "indiv": {
            "Points": {"summary": "Points", "plots": []},
            "Hand": {"summary": "Hands", "plots": []},
            "Solo": {"summary": "Solo", "plots": []},
            "Others": {"summary": "Others", "plots": []},
        },
        "team": {
            "Points": {"summary": "Points", "plots": []},
            "Hand": {"summary": "Hands", "plots": []},
            "Others": {"summary": "Others", "plots": []},
        },
    }
    if processHighscores:
        highscoreFilenames.extend(plotHighscores(playerHighscoreDict, highscoreFolder, False))
        highscoreFilenames.extend(plotHighscores(teamHighscoreDict, highscoreFolder, True))

        relpath = highscoreFolder.relative_to("build")

        categories = {"indiv": ["Points", "Hand", "Solo"], "team": ["Points", "Hand"]}

        # for pType in categories:
        for f in highscoreFilenames:
            # check if its a indiv or team plot
            if f.startswith("Team"):
                pType = "team"
            else:
                pType = "indiv"
            foundCat = False
            for cat in categories[pType]:
                if cat.lower() in f.lower():
                    foundCat = True
                    highscoreRows[pType][cat]["plots"].append(relpath / f)
                    break
            # add plot to "others"
            if not foundCat:
                highscoreRows[pType]["Others"]["plots"].append(relpath / f)

    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(pathlib.Path(__file__).parent.resolve()),
        autoescape=True,
        undefined=jinja2.StrictUndefined,
    )

    template = env.get_template("index.html.j2")

    with open(outpath / "index.html", "w", encoding="utf-8") as f:
        f.write(template.render(rows=HTMLRows, highscoreRows=highscoreRows))


if __name__ == "__main__":
    main()
