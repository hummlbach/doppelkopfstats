from doppelkopfstats import *
from general import StatsTypes


class Highscore:
    def __init__(self, type, title, ylabel, dataIsFloat):
        self.type = type
        self.title = title
        self.ylabel = ylabel
        self.dataIsFloat = dataIsFloat

        self.isInitialized = {
            StatsTypes.OVERALL: False,
            StatsTypes.YEAR: False,
            StatsTypes.MONTH: False,
        }
        self.data = dict()  # key: StatsTypes, value: list of tuples [name, value, dateInfo]
        self.noOfEntries = dict()  # key: StatsTypes, value: no of entries for stat of date

    def addDateAndEntries(self, dateType, entries):
        if dateType in self.data:
            raise Exception(
                f"ERROR: DateType {dateType} is already present in Highscore dictionary for data of type {self.type}"
            )
        self.data[dateType] = list()
        if dateType in self.noOfEntries:
            raise Exception(
                f"ERROR: DateType {dateType} is already present in Highscore dictionary for number of entries"
            )
        self.noOfEntries[dateType] = entries
        self.isInitialized[dateType] = True

    def addEntryToDate(self, dateType, player, value, dateInfo):
        self.data[dateType].append((player, value, dateInfo))

    def sortData(self):
        for k, v in self.data.items():
            v.sort(key=lambda x: x[1], reverse=True)

    def getData(self):
        data = dict()
        self.sortData()
        for key in self.data:
            data[key] = self.data[key][: self.noOfEntries[key]]
        return data
